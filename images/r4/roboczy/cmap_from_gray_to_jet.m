close all
basename='meddiff';
extension='png';
I=imread([basename '.' extension]);

cm=colormap(jet(256));

I2=zeros(size(I,1), size(I,2), 3);

for i=1:size(I,1)
    for j=1:size(I,2)
        if (I(i,j)>0)            
            I2(i,j,1)= cm(I(i,j)+1,1);
            I2(i,j,2)= cm(I(i,j)+1,2);
            I2(i,j,3)= cm(I(i,j)+1,3);
        else
            I2(i,j,1)= 0;
            I2(i,j,2)= 0;
            I2(i,j,3)= 0;
        end
    end
end

imshow(I2);

imwrite(I2, [basename '_color' '.' extension]);





