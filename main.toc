\contentsline {chapter}{\numberline {1}Wst\k ep}{8}
\contentsline {section}{\numberline {1.1}Wprowadzenie}{8}
\contentsline {section}{\numberline {1.2}Cel i teza rozprawy}{9}
\contentsline {section}{\numberline {1.3}Przegl\k ad tre\'sci rozprawy}{10}
\contentsline {chapter}{\numberline {2}Obszar problemowy rozprawy}{12}
\contentsline {section}{\numberline {2.1}Sensory wizyjne w zadaniu rekonstrukcji sceny robota krocz\k acego}{12}
\contentsline {subsection}{\numberline {2.1.1}Kamery monokularowe}{13}
\contentsline {subsection}{\numberline {2.1.2}Uk\IeC {\l }ady z o\'swietleniem strukturalnym}{14}
\contentsline {subsection}{\numberline {2.1.3}Sensor Kinect/Xtion}{15}
\contentsline {subsection}{\numberline {2.1.4}Kamery w uk\IeC {\l }adzie stereowizyjnym}{15}
\contentsline {subsection}{\numberline {2.1.5}Skanery laserowe}{16}
\contentsline {subsection}{\numberline {2.1.6}Kamery TOF}{17}
\contentsline {section}{\numberline {2.2}Istniej\k ace rozwi\k azania w zakresie rekonstrukcji sceny robota krocz\k acego}{19}
\contentsline {chapter}{\numberline {3}Aktywny system wizyjny z o\'swietleniem strukturalnym}{24}
\contentsline {section}{\numberline {3.1}System wizyjny z o\'swietleniem strukturalnym}{24}
\contentsline {section}{\numberline {3.2}Algorytm rekonstrukcji geometrii sceny tr\'ojwymiarowej}{25}
\contentsline {subsection}{\numberline {3.2.1}Pomiary przy u\.zyciu uk\IeC {\l }adu kamera\nobreakspace {}--\nobreakspace {}o\'swietlacz}{25}
\contentsline {subsection}{\numberline {3.2.2}Ekstrakcja \'sladu \'swietlnego na obrazie}{28}
\contentsline {section}{\numberline {3.3}Metoda kalibracji systemu i analiza niepewno\'sci pomiaru wielko\'sci geometrycznych}{32}
\contentsline {subsection}{\numberline {3.3.1}Kalibracja uk\IeC {\l }adu kamera -- o\'swietlacz}{32}
\contentsline {subsection}{\numberline {3.3.2}Analiza niepewno\'sci pomiaru wielko\'sci geometrycznych}{34}
\contentsline {subsubsection}{Przyczyny b\IeC {\l }\k ed\'ow pomiaru}{34}
\contentsline {subsubsection}{Model niepewno\'sci}{35}
\contentsline {subsubsection}{Niepewno\'s\'c pomiaru w zale\.zno\'sci od po\IeC {\l }o\.zenia punktu wzgl\k edem kamery}{37}
\contentsline {subsubsection}{Niepewno\'s\'c pomiaru w zale\.zno\'sci od doboru parametr\'ow \\p\IeC {\l }aszczyzny arkusza \'swietlnego}{38}
\contentsline {subsubsection}{Wp\IeC {\l }yw b\IeC {\l }\k ed\'ow kalibracji na niepewno\'s\'c pomiar\'ow}{41}
\contentsline {section}{\numberline {3.4}Optymalizacja konfiguracji systemu wizyjnego w zadaniu akwizycji mapy terenu}{44}
\contentsline {chapter}{\numberline {4}Zastosowania aktywnego systemu wizyjnego w rekonstrukcji sceny robota krocz\k acego}{50}
\contentsline {section}{\numberline {4.1}Rekonstrukcja geometrii schod\'ow wraz z weryfikacj\k a eksperymentaln\k a}{50}
\contentsline {subsection}{\numberline {4.1.1}Percepcja we wspinaczce po schodach}{50}
\contentsline {subsubsection}{Pomiary przy u\.zyciu skanera laserowego}{51}
\contentsline {subsubsection}{Pomiary przy u\.zyciu systemu z o\'swietleniem strukturalnym}{53}
\contentsline {section}{\numberline {4.2}Rekonstrukcja mapy terenu, wraz z weryfikacj\k a eksperymentaln\k a}{59}
\contentsline {subsection}{\numberline {4.2.1}Wst\k ep}{59}
\contentsline {subsection}{\numberline {4.2.2}Algorytm aktualizacji mapy}{59}
\contentsline {subsection}{\numberline {4.2.3}Wyniki eksperymentalne}{62}
\contentsline {chapter}{\numberline {5}Pasywny system stereowizyjny}{65}
\contentsline {section}{\numberline {5.1}Metoda rekonstrukcji sceny 3D na podstawie g\k estej mapy g\IeC {\l }\k ebi}{65}
\contentsline {subsection}{\numberline {5.1.1}Wykorzystywany system stereowizyjny}{65}
\contentsline {subsection}{\numberline {5.1.2}Model systemu stereowizyjnego}{66}
\contentsline {subsection}{\numberline {5.1.3}Pomiary przy u\.zyciu kamery Videre Design STOC}{72}
\contentsline {subsubsection}{Wbudowany algorytm przetwarzania danych stereowizyjnych}{72}
\contentsline {subsubsection}{Postprocessing danych}{74}
\contentsline {section}{\numberline {5.2}Analiza niepewno\'sci pomiaru wielko\'sci geometrycznych}{77}
\contentsline {subsection}{\numberline {5.2.1}Wst\k ep}{77}
\contentsline {subsection}{\numberline {5.2.2}Model niepewno\'sci}{80}
\contentsline {subsection}{\numberline {5.2.3}Fizyczna interpretacja niepewno\'sci}{83}
\contentsline {section}{\numberline {5.3}Metoda budowy mapy terenu, wraz z weryfikacj\k a eksperymentaln\k a}{83}
\contentsline {subsection}{\numberline {5.3.1}Wst\k ep}{83}
\contentsline {subsection}{\numberline {5.3.2}Algorytm budowy mapy terenu}{84}
\contentsline {subsection}{\numberline {5.3.3}Weryfikacja eksperymentalna}{89}
\contentsline {chapter}{\numberline {6}Jednoczesne wykorzystanie danych z wielu sensor\'ow wizyjnych}{95}
\contentsline {section}{\numberline {6.1}Wst\k ep}{95}
\contentsline {section}{\numberline {6.2}Skaner Laserowy Hokuyo URG--04LX}{96}
\contentsline {subsection}{\numberline {6.2.1}Przekszta\IeC {\l }canie pomiar\'ow do uk\IeC {\l }adu zwi\k azanego z map\k a}{96}
\contentsline {subsection}{\numberline {6.2.2}Niepewno\'s\'c pomiaru wielko\'sci geometrycznych za pomoc\k a skanera laserowego Hokuyo URG--04LX}{97}
\contentsline {section}{\numberline {6.3}Metoda kalibracji uk\IeC {\l }adu wielosensorycznego z\IeC {\l }o\.zonego z kamery stereowizyjnej i\nobreakspace {}skanera laserowego 2D}{98}
\contentsline {subsection}{\numberline {6.3.1}Eksperyment kalibracyjny}{99}
\contentsline {subsection}{\numberline {6.3.2}Filtracja danych}{100}
\contentsline {subsection}{\numberline {6.3.3}Identyfikacja parametr\'ow}{101}
\contentsline {subsection}{\numberline {6.3.4}Wyniki}{103}
\contentsline {subsection}{\numberline {6.3.5}Niepewno\'s\'c kalibracji uk\IeC {\l }adu wielosensorycznego}{105}
\contentsline {section}{\numberline {6.4}Budowa mapy terenu przy u\.zyciu pasywnej stereowizji i skanera laserowego}{108}
\contentsline {chapter}{\numberline {7}Metoda samolokalizacji robota krocz\k acego na podstawie g\k estej mapy g\IeC {\l }\k ebi}{111}
\contentsline {section}{\numberline {7.1}Algorytm samolokalizacji}{111}
\contentsline {subsection}{\numberline {7.1.1}Wst\k ep}{111}
\contentsline {subsection}{\numberline {7.1.2}Przygotowanie danych}{114}
\contentsline {subsection}{\numberline {7.1.3}Dopasowanie map}{117}
\contentsline {section}{\numberline {7.2}Wyniki eksperymentalne}{119}
\contentsline {chapter}{\numberline {8}Wnioski}{124}
\contentsline {section}{\numberline {8.1}Wnioski w kontek\'scie dotychczasowego stanu wiedzy}{124}
\contentsline {section}{\numberline {8.2}Podsumowanie osi\k agni\k etych wynik\'ow}{126}
\contentsline {section}{\numberline {8.3}Kierunki dalszych bada\'n}{127}
